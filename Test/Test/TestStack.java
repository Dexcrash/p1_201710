package Test;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class TestStack extends TestCase {

	private Stack<String> pila;

	public void setupEscenario1() {
		pila = new Stack<String>();
	}

	public void setupEscenario2() {
		pila = new Stack<String>();
		pila.push("Es");
		pila.push("Un");
		pila.push("Dia");
		pila.push("Muy");
		pila.push("Soleado");
	}

	public void setupEscenario3() {
		pila = new Stack<String>();
		pila.push("Es");
		pila.push("Un");
		pila.push("Dia");
		pila.push("Muy");
		pila.push("Soleado");
		pila.pop();
		pila.pop();
	}

	public void testpush1() {
		setupEscenario1();
		pila.push("Hola");
		assertEquals("El elemento no es correcto", "Hola", pila.pop());
	}

	public void testpush2() {
		setupEscenario2();
		assertEquals(pila.pop(), "Soleado");
		pila.push("Demasiado");
		pila.push("Soleado");
		assertEquals(pila.pop(), "Soleado");
	}

	public void testpop1() {
		setupEscenario1();
		assertNull("No deberia haber ningun objeto guardado.", pila.pop());
	}

	public void testpop2() {
		setupEscenario3();
		assertEquals(pila.pop(), "Dia");
	}

	public void testIsEmpty1() {
		setupEscenario1();
		assertTrue("La pila deberia estar vacia", pila.isEmpty());
	}

	public void testIsEmpty2() {
		setupEscenario2();
		assertFalse("La pila no dberia estar vacia", pila.isEmpty());
		setupEscenario3();
		assertFalse("La pila no dberia estar vacia", pila.isEmpty());
	}

	public void testSize1() {
		setupEscenario1();
		assertEquals(pila.size(), 0);
	}

	public void testSize2() {
		setupEscenario2();
		assertEquals(pila.size(), 5);
		setupEscenario3();
		assertEquals(pila.size(), 3);
	}

}
