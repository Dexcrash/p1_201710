package Test;

import java.util.ArrayList;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class TestListaDoblementeEnlazada extends TestCase{


	private ListaDobleEncadenada<String> lista;
	private ArrayList<String> modelo;


	public void setupEscenario1( )
	{
		lista = new ListaDobleEncadenada<String>();
	}
	
	public void setupEscenario2(){
		lista = new ListaDobleEncadenada<String>();
		lista.agregarElementoFinal("Yo");
	}
	
	public void setupEscenario3(){
		lista = new ListaDobleEncadenada<String>();
		lista.agregarElementoFinal("Yo");
		lista.agregarElementoFinal("Tu");
		lista.agregarElementoFinal("El");
		lista.agregarElementoFinal("Ella");
		lista.agregarElementoFinal("Ustedes");
		lista.agregarElementoFinal("Nosotros");
		lista.agregarElementoFinal("Eso");
		lista.agregarElementoFinal("Esa");
		lista.agregarElementoFinal("Vos");
		lista.agregarElementoFinal("Vosotros");
	}
	
	public void setupEscenario4()
	{
		lista = new ListaDobleEncadenada<String>();
		modelo = new ArrayList<>();
		for(int i =0; i< 1500 ;i++){
			modelo.add(""+Math.random()*3000);
		}
		for(String s : modelo){
			lista.agregarElementoFinal(s);
		}
	} 
	
	public void testAgregarAlFinal1(){
		setupEscenario1();
		assertNull("No deberia haber ningun objeto guardado.", lista.darElemento(0));
	}
	
	public void testAgregarAlFinal2(){
		setupEscenario2();
		assertEquals("El elemento es incorrecto", "Yo", lista.darElemento(lista.darNumeroElementos()-1));
		lista.agregarElementoFinal("Tu");
		assertEquals("El elemento es incorrecto", "Tu", lista.darElemento(lista.darNumeroElementos()-1));
		lista.agregarElementoFinal("Lo");
		assertEquals("El elemento es incorrecto", "Lo", lista.darElemento(lista.darNumeroElementos()-1));
		lista.agregarElementoFinal("Uniandes");
		assertEquals("El elemento es incorrecto", "Uniandes", lista.darElemento(lista.darNumeroElementos()-1));
	}
	
	public void testAgregarAlFinal3(){
		setupEscenario4();
		assertEquals("El elemento no es correcto", modelo.get(modelo.size()-1) , lista.darElemento(lista.darNumeroElementos()-1) );
	}
	
	
	public void testDarElemento1(){
		setupEscenario2();
		assertEquals("El elemento es incorrecto", "Yo", lista.darElemento(0));
	}
	
	public void testDarElemento2(){
		setupEscenario3();
		assertEquals("El elemento es incorrecto", "Yo", lista.darElemento(0));
		assertEquals("El elemento es incorrecto", "El", lista.darElemento(2));
		assertEquals("El elemento es incorrecto", "Ustedes", lista.darElemento(4));
		assertEquals("El elemento es incorrecto", "Eso", lista.darElemento(6));
		assertEquals("El elemento es incorrecto", "Vos", lista.darElemento(8));
		assertEquals("El elemento es incorrecto", "Yo", lista.darElemento(0));
	}
	
	public void testDarNumeroElementos1(){
		setupEscenario2();
		assertEquals("El tama�o no es el adecuado", 1, lista.darNumeroElementos());
	}
	
	public void testDarNumeroElementos2(){
		setupEscenario3();
		assertEquals("El tama�o no es el adecuado",10 , lista.darNumeroElementos());
		lista.agregarElementoFinal("Uniandes");
		assertEquals("El tama�o no es el adecuado",11 , lista.darNumeroElementos());
		lista.agregarElementoFinal("LOL");
		assertEquals("El tama�o no es el adecuado",12 , lista.darNumeroElementos());
	}
	
	public void testDarEliminarElemento1(){
		setupEscenario1();
		assertNull("El tama�o no es el adecuado", lista.eliminarElemento(20));
	}
	public void testDarEliminarElemento2(){
		setupEscenario2();
		lista.eliminarElemento(0);
		assertNull("El tama�o no es el adecuado", lista.darElemento(0));
	}
	
	
	
	public void testEliminarElemento3(){
		setupEscenario3();
		lista.eliminarElemento(2);
		assertEquals("El tama�o no es el adecuado","Ella" , lista.darElemento(2));
		lista.eliminarElemento(4);
		assertEquals("El tama�o no es el adecuado","Eso" , lista.darElemento(4));
		lista.eliminarElemento(6);
		assertEquals("El tama�o no es el adecuado","Vosotros" , lista.darElemento(6));
	}

}
