package Test;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class TestQueue extends TestCase {

	private Queue<String> fila;
	
	public void setupEscenario1(){
		fila = new Queue<String>();	
	}
	
	public void setupEscenario2(){
		fila = new Queue<String>();
		fila.enqueue("Es");
		fila.enqueue("Un");
		fila.enqueue("Dia");
		fila.enqueue("Muy");
		fila.enqueue("Soleado");
	}
	
	public void setupEscenario3(){
		fila = new Queue<String>();
		fila.enqueue("Es");
		fila.enqueue("Un");
		fila.enqueue("Dia");
		fila.enqueue("Muy");
		fila.enqueue("Soleado");
		fila.dequeue();
		fila.dequeue();
	}
	
	public void testEnqueue1(){
		setupEscenario1();
		fila.enqueue("Hola");
		assertEquals(fila.dequeue(), "Hola");
	}
	
	public void testEnqueue2(){
		setupEscenario2();
		assertEquals(fila.dequeue(), "Es");
		fila.enqueue("Demasiado");
		fila.enqueue("Soleado");
		assertEquals(fila.dequeue(), "Un");	
	}
	
	public void testDequeue1(){
		setupEscenario1();
		assertNull("No deberia haber ningun objeto guardado.", fila.dequeue());
	}
	
	public void testDequeue2(){
		setupEscenario3();
		assertEquals(fila.dequeue(), "Dia");
	}

	public void testIsEmpty1() {
		setupEscenario1();
		assertTrue("La fila deberia estar vacia", fila.isEmpty());
	}

	public void testIsEmpty2() {
		setupEscenario2();
		assertFalse("La fila no dberia estar vacia", fila.isEmpty());
		setupEscenario3();
		assertFalse("La fila no dberia estar vacia", fila.isEmpty());
	}
	
	public void testSize1(){
		setupEscenario1();
		assertEquals(fila.size(), 0);
	}
	public void testSize2(){
		setupEscenario2();
		assertEquals(fila.size(), 5);
		setupEscenario3();
		assertEquals(fila.size(), 3);
	}
		
}
