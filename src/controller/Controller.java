package controller;

import com.sun.org.apache.xml.internal.security.encryption.AgreementMethod;

import api.ISistemaRecomendacionPeliculas;
import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.logic.SistemaRecomendacionpeliculas;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioGenero;

public class Controller {

	
	private static ISistemaRecomendacionPeliculas sr = new SistemaRecomendacionpeliculas();
	
	
	public static boolean cargarPeliculas(){
		return sr.cargarPeliculasSR("./data/movies.csv");
	}
	
	public static boolean cargarRatings(){
		return sr.cargarRatingsSR("./data/ratings.csv");
	}
	public static boolean cargarTags(){
		return sr.cargarTagsSR("./data/tags.csv");
	}
	public static int sizePeliculas(){
		return sr.sizeMoviesSR();
	}
	public static int sizeUsers(){
		return sr.sizeUsersSR();
	}
	public static int sizeTags(){
		return sr.sizeTagsSR();
	}
	
	public static ILista<VOGeneroPelicula> peliculasPopulares(int n){
		ILista<VOGeneroPelicula> l  = sr.peliculasPopularesSR(n);
		for(VOGeneroPelicula g : l){
			System.out.println("---------------Genero: "+ g.getGenero()+" ---------------");
			for(VOPelicula p: g.getPeliculas()){
				System.out.println(p.getTitulo());
			}
		}
		return l;
	}
	
	public static ILista<VOPelicula> catalogoPeliculasOrdenado(){
		ILista<VOPelicula> pelis =sr.catalogoPeliculasOrdenadoSR();
		for(VOPelicula p: pelis){
			System.out.println(p.getAgnoPublicacion() + " : "+ p.getTitulo()+ " : " + p.getPromedioRatings());
		}
		return pelis;
	}
	
	public static ILista<VOGeneroPelicula> recomedarGenero(){
		
		return sr.recomendarGeneroSR();
	}
	public static ILista<VOGeneroPelicula> opinionRatingsGenero(){
		ILista<VOGeneroPelicula> l  =sr.opinionRatingsGeneroSR();
		for(VOGeneroPelicula g : l){
			System.out.println("---------------Genero: "+ g.getGenero()+" ---------------");
			for(VOPelicula p: g.getPeliculas()){
				System.out.println(p.getAgnoPublicacion() + " : "+ p.getTitulo()+ " : " + p.getPromedioRatings());
			}
		}
		return l;
	}
	
	public static ILista<VOPeliculaPelicula> recomendarPeliculas(String rutaRecomendacion, int n){
		 ILista<VOPeliculaPelicula> l = sr.recomendarPeliculasSR(rutaRecomendacion, n);
		 for(VOPeliculaPelicula pp : l){
			 VOPelicula p = pp.getPelicula();
			 System.out.println(p.getAgnoPublicacion() + " : "+ p.getTitulo()+ " : " + p.getPromedioRatings());
				for(VOPelicula peli: pp.getPeliculasRelacionadas()){
					System.out.println(peli.getAgnoPublicacion() + " : "+ peli.getTitulo()+ " : " + peli.getPromedioRatings());
				}
		 }
		return l;
	}
	
	public static ILista<VORating> ratingsPelicula(long idPelicula){
		 ILista<VORating> l = sr.ratingsPeliculaSR(idPelicula);
		 for(VORating r : l){
			 System.out.println(r.getIdPelicula() + " : " + r.getTimestamp());
		 }
		return l;
	}
	
	public static ILista<VOGeneroUsuario> usuariosActivos(int n){
		return sr.usuariosActivosSR(n);
	}
	
	public static ILista<VOUsuario> catalogoUsuariosOrdenado(){
		return sr.catalogoUsuariosOrdenadoSR();
	}
	
	public static ILista<VOGeneroPelicula> recomendarTagsGenero(int n){
		return sr.recomendarTagsGeneroSR(n);
	}
	
	public static ILista<VOUsuarioGenero> opinionTagsGnero(){
		return sr.opinionTagsGeneroSR();
	}
	
	public static ILista<VOPeliculaUsuario> recomendarUsuarios(String rutaRecomendacion, int n){
		return sr.recomendarUsuariosSR(rutaRecomendacion, n);
	}
	
	public static ILista<VOTag> tagsPelicula(int idPelicula){
		return sr.tagsPeliculaSR(idPelicula);
	}
	
	public static ILista<VOOperacion> darHistorialOperaciones(){
		return sr.darHistoralOperacionesSR();
	}
	
	public static void limpiraHistorialOperaciones(){
		sr.limpiarHistorialOperacionesSR();
	}
	
	public static ILista<VOOperacion> darUltimasOperaciones(){
		return sr.darHistoralOperacionesSR();
	}
	
	public static ILista<VOOperacion> borrarUltimasOperaciones(){
		return sr.darHistoralOperacionesSR();
	}

	public static void printAll() {
		// TODO Auto-generated method stub
		sr.printAll();
	}
	
	
	
	
}
