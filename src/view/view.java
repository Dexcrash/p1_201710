package view;

import java.util.Scanner;

import controller.Controller;

public class view {
	
	public static void main(String[] args) {
	
		final String RUTA_PELIS = "";
		
		Scanner scan = new Scanner(System.in);
		boolean f = false;
		while(!f){
			printMenu();
			
			int option = scan.nextInt();
			
			switch(option){
			
			case 1:
				System.out.println(Controller.cargarPeliculas());
				break;
			case 2:
				System.out.println(Controller.cargarRatings());
				break;
			case 3:
				System.out.println(Controller.cargarTags());
				break;
			case 4:
				System.out.println(Controller.sizePeliculas());
				break;
			case 5:
				System.out.println(Controller.sizeUsers());
				break;
			case 6:
				System.out.println(Controller.sizeTags());
				break;
			case 7:
				int n = scan.nextInt();
				System.out.println(Controller.peliculasPopulares(n));
				break;
			case 8:
				System.out.println(Controller.catalogoPeliculasOrdenado());
				break;
			case 9:
				System.out.println(Controller.recomedarGenero());
				break;
			case 10:
				System.out.println(Controller.opinionRatingsGenero());
				break;
			case 11:
				int n1 = scan.nextInt();
				System.out.println(Controller.recomendarPeliculas("./data/recomendacion.csv", n1));
				break;
			case 12:
				int n2 = scan.nextInt();
				System.out.println(Controller.ratingsPelicula(n2));
				break;
			case 13:
				int n3 = scan.nextInt();
				System.out.println(Controller.usuariosActivos(n3));
				break;
			case 14:
				System.out.println(Controller.catalogoUsuariosOrdenado());
				break;
			case 15:
				int n4 = scan.nextInt();
				System.out.println(Controller.recomendarTagsGenero(n4));
				break;
			case 16:
				System.out.println(Controller.opinionTagsGnero());
				break;
			case 17:
				int n5 = scan.nextInt();
				System.out.println(Controller.recomendarUsuarios(RUTA_PELIS, n5));
				break;
			case 18:
				int n6 = scan.nextInt();
				System.out.println(Controller.tagsPelicula(n6));
				break;
			case 19:
				System.out.println(Controller.darHistorialOperaciones());
				break;
			case 20:
				System.out.println(Controller.darUltimasOperaciones());
				break;
			case 21:
				Controller.printAll();
				break;
			}
		}
	}
	
	public static void printMenu(){
		System.out.println("------------------------------------------------------------");
		System.out.println("--------------ISIS 1206 - Estructuras de Datos--------------");
		System.out.println("----------------------------Proyecto 1------------------------");
		System.out.println("1. Cargar Peliculas");
		System.out.println("2. Cargar Ratings");
		System.out.println("3. Cargar Tags");
		System.out.println("4. Tama�o Peliculas");
		System.out.println("5. Tama�o Usuarios");
		System.out.println("6. Tama�o Tags");
		System.out.println("7. Peliculas populares(A1)");
		System.out.println("8. Catalogo Peliculas ordenado(A2)");
		System.out.println("9. Recomendar Generos(A3)");
		System.out.println("10. Opinion Ratings Genero(A4)");
		System.out.println("11. Recomendar Peliculas(A5)");
		System.out.println("12. Rating Pelicula(A6)");
		System.out.println("13. Usuarios Activos(B1)");
		System.out.println("14. CatalogoUsuarios Activos(B2)");
		System.out.println("15. Recomendar Tags Genero(B3)");
		System.out.println("16. Opinion Tags Genero(B4)");
		System.out.println("17. Recomendar Usuarios (B5)");
		System.out.println("18. Tags peliculas (B6)");
		System.out.println("19. Dar Historial Op (C1)");
		System.out.println("20. Dar Ultimas Operaciones(C3)");
		System.out.println("21. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
}
