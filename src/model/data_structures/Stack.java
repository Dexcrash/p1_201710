package model.data_structures;

public class Stack <T> implements IStack<T>{

	private ListaDobleEncadenada<T> lista;
	
	public Stack() {
		lista = new ListaDobleEncadenada<T>();
	}

	@Override
	public void push(T elemento) {
		// TODO Auto-generated method stub
		lista.agregarElementoFinal(elemento);
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		return lista.eliminarElemento(lista.darNumeroElementos()-1);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos()==0;
	}
	
}
