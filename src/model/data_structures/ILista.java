package model.data_structures;

import java.util.Iterator;

public interface ILista<T> extends Iterable<T> {
	
	public void agregarElementoFinal(T elem);
	
	public T darElemento(int pos);
	
	public T eliminarElemento(int pos);
	
	public int darNumeroElementos();

	public Iterator<T> iterator();
	
	public void setItem(T e,int pos);
	
}
