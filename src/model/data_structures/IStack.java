package model.data_structures;

public interface IStack <T>{
	
	public void push(T elemento);
	
	
	
	/**
	 * Saca el primer elemento en el Stack
	 * @return elemento tipo T 
	 */
	public T pop();
	
	public int size();
	
	public boolean isEmpty();
	
	
	
	
}
