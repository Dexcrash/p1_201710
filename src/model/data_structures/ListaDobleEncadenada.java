package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaDobleEncadenada.NodoDoble;

public class ListaDobleEncadenada<T> implements ILista<T> {

	NodoDoble<T> first;
	NodoDoble<T> current;
	int size;
	



	private class myIterator<T> implements Iterator<T> {

        private NodoDoble<T> current;

        public  myIterator(NodoDoble<T> first) {
            current = first;
        }

        public boolean hasNext() {

            return current != null;
        }

        public T next() {
            if(hasNext()) {
                T item = current.item;
                current = current.next;
                return item;
            }
            return null;
        }
    }
    
	static class NodoDoble<T>{
		NodoDoble<T> next;
		NodoDoble<T> previous;
		T item;
		public NodoDoble(NodoDoble<T> next,NodoDoble<T> previous, T e) {
			// TODO Auto-generated constructor stub
			this.next = next;
			this.previous = previous;
			this.item = e;
		}
		public NodoDoble<T> getNext() {
			return next;
		}
		public void setNext(NodoDoble<T> next) {
			this.next = next;
		}
		public T getItem() {
			return item;
		}
		public void setItem(T item) {
			this.item = item;
		}
		public NodoDoble<T> getPrevious() {
			return previous;
		}
		public void setPrevious(NodoDoble<T> previous) {
			this.previous = previous;
		}
		
	}
	

	public ListaDobleEncadenada() {
		// TODO Auto-generated constructor stub
		first = current = null;
		size =0;
	}
	
	
	public ListaDobleEncadenada(T[] s){
		this.first = this.current = null;
		size=0;
		for(T si : s){
			agregarElementoFinal(si);
		}
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new myIterator<>(first);
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(first == null){
			first = current= new NodoDoble<T>(null,null, elem);
			size++;
			return;
		}
		
		current = first;
		while(current.next!=null)
			current = current.next;
		current.setNext(new NodoDoble<T>(null,current, elem));
		current = current.next;
		size++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		if(pos == 0 && first !=null)
			return first.getItem();
		if(pos<size && pos>0){
			current = first.getNext();
			pos--;
			while(pos>0){
				current = current.getNext();
				pos--;
			}
			return current.getItem();
		}
		return null;
	}

	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		T res = null;
		if(pos == 0 && first !=null){
			res = first.getItem();
			first = first.getNext();
			size--;
		}
		if(pos<size && pos>0){
			current = first.getNext();
			pos--;
			while(pos>0){
				current = current.getNext();
				pos--;
			}
			res = current.getItem();
			size--;
			NodoDoble<T> prev = current.getPrevious();
			current = current.getNext();
			prev.setNext(current);
			if(current!=null)
				current.setPrevious(prev);
		}
		return res;
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void setItem(T e, int pos) {
		// TODO Auto-generated method stub
		if(pos == 0 && first !=null){
			first.setItem(e);
		}
		if(pos<size && pos>0){
			current = first.getNext();
			pos--;
			while(pos>0){
				current = current.getNext();
				pos--;
			}
			current.setItem(e);
		}
	}
	
		
}
