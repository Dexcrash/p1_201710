package model.data_structures;

public class Queue <T>implements IQueue<T> {

	private ListaDobleEncadenada<T> lista;
	
	public Queue() {
		// TODO Auto-generated constructor stub
		lista = new ListaDobleEncadenada<T>();
	}
	
	@Override
	public void enqueue(T elemento) {
		// TODO Auto-generated method stub
		lista.agregarElementoFinal(elemento);
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		return lista.eliminarElemento(0);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos()==0;
	}

	
}
