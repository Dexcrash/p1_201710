package model.vo;

import com.sun.jmx.snmp.Timestamp;

public class VORating implements Comparable<VORating>{
	
	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timestamp;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamo) {
		this.timestamp = timestamo;
	}
	@Override
	public int compareTo(VORating arg0) {
		// TODO Auto-generated method stub
		long i = timestamp-arg0.timestamp;
		if(i!=0){
			return i<0?1:-1;
		}
		return 0;
	}
	

}
