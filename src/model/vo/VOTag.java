package model.vo;

public class VOTag implements Comparable<VOTag>{
	private long moveId;
	private long userId;
	private String tag;
	private long timestamp;
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public long getMoveId() {
		return moveId;
	}
	public void setMoveId(long moveId) {
		this.moveId = moveId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	@Override
	public int compareTo(VOTag o) {
		// TODO Auto-generated method stub
		long i = timestamp -	o.timestamp;
		if(i!=0){
			return i<0?1:-1;
		}
		return 0;
	}
}
