package model.vo;

public class VOUsuario implements Comparable<VOUsuario> {

	private long idUsuario;	
	private long primerTimestamp;
	private int numRatings;
	private double diferenciaOpinion;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	@Override
	public int compareTo(VOUsuario o) {
		// TODO Auto-generated method stub
		long pTime = primerTimestamp - o.getPrimerTimestamp();
		int nRatings = numRatings - o.getNumRatings();
		long id = idUsuario - o.idUsuario;
		if(pTime!=0)return pTime<0?-1:1;
		if(nRatings!=0)return nRatings<0?-1:1;
		if(id!=0)return id<0?-1:1;
		return 0;
	}
	
}
