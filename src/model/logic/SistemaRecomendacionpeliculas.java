package model.logic;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import api.ISistemaRecomendacionPeliculas;
import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;
import sun.security.util.Length;

public class SistemaRecomendacionpeliculas implements ISistemaRecomendacionPeliculas{
	
	private ListaDobleEncadenada<VOPelicula> listaPeliculas;
	private ListaDobleEncadenada<VORating> listaRatings;
	private ListaDobleEncadenada<VOTag> listaTags;
	private ListaDobleEncadenada<VOGeneroPelicula> listaGeneros;
	private ListaDobleEncadenada<VOUsuario> listaUsuarios;
	private QuickSort<VOPelicula> quick;
	private QuickSort<VORating> quick2;
	private QuickSort<VOUsuario> quick3;
	private QuickSort<VOTag> quick4;
	private Stack<VOOperacion> operaciones;

	
	public SistemaRecomendacionpeliculas() {
		// TODO Auto-generated constructor stub
		listaPeliculas = new ListaDobleEncadenada<>();
		listaRatings = new ListaDobleEncadenada<>();
		listaTags = new ListaDobleEncadenada<>();
		listaGeneros = new ListaDobleEncadenada<>();
		quick = new QuickSort<>();
		quick2 = new QuickSort<>();
		quick3= new QuickSort<>();
		quick4 = new QuickSort<>();
		operaciones = new Stack<>();
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		boolean res = false;
		BufferedReader buff = null;
		String sepCvs = ",";
		String sepGen = "\\|";
		String line = "";
		int cont =0;
		try {
			buff = new BufferedReader(new FileReader(rutaPeliculas));
			line = buff.readLine();
			while ((line = buff.readLine()) != null) {
				String[] datos = line.split(sepCvs);
				long movieId = Long.parseLong(datos[0]);
				String generos = datos[datos.length - 1];
				String nombre = "";
				for (int i = 1; i < datos.length - 1; i++) {
					nombre = nombre + datos[i];
				}

				if (nombre.startsWith("\"") && nombre.endsWith("\"")) {
					nombre = nombre.substring(1, nombre.length() - 1);
				}

				int a�o = -1;

				if (nombre.endsWith(")")) {
					Character g = nombre.charAt(nombre.length() - 2);
					if (g.isDigit(g)) {
						a�o = Integer.parseInt(nombre.substring(nombre.length() - 5, nombre.length() - 1));
					} else {
						a�o = Integer.parseInt(nombre.substring(nombre.length() - 6, nombre.length() - 2));
					}
				}

				nombre = nombre.substring(0, nombre.length() - 6);
				VOPelicula p = new VOPelicula();
				p.setAgnoPublicacion(a�o);
				String[] s = generos.split(sepGen);
				p.setGenerosAsociados(new ListaEncadenada<>(s));
				p.setTitulo(nombre);
				p.setIdPelicula(movieId);
				p.setNumeroRatings(0);
				p.setNumeroTags(0);
				p.setPromedioRatings(0);
				p.setTagsAsociados(new ListaDobleEncadenada<>());
				listaPeliculas.agregarElementoFinal(p);
				cont++;
				if(cont%1000 == 0)System.out.println("Loading...");
			}
			res = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		agregarOperacionSR("cargarPeliculas", time, System.currentTimeMillis());
		return res;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		boolean res = false;
		BufferedReader buff = null;
		String sepCvs = ",";
		String line = "";
		int cont =0;
		try {
			buff = new BufferedReader(new FileReader(rutaRatings));
			line = buff.readLine();
			while ((line = buff.readLine()) != null) {
				String[] datos = line.split(sepCvs);
				long userId = Long.parseLong(datos[0]);
				long movieId = Long.parseLong(datos[1]);
				double rating = Double.parseDouble(datos[2]);
				long timestamp = Long.parseLong(datos[3]);
				
				VORating r = new VORating();
				r.setIdUsuario(userId);
				r.setIdPelicula(movieId);
				r.setRating(rating);
				r.setTimestamp(timestamp);
				listaRatings.agregarElementoFinal(r);
				cont++;
				if(cont%5000 == 0)System.out.println("Loading...");
			}
			res = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		agregarOperacionSR("cargarRatings", time, System.currentTimeMillis());
		return res;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		boolean res = false;
		BufferedReader buff = null;
		String sepCvs = ",";
		String line = "";
		int cont =0;
		try {
			buff = new BufferedReader(new FileReader(rutaTags));
			line = buff.readLine();
			while ((line = buff.readLine()) != null) {
				String[] datos = line.split(sepCvs);
				long userId = Long.parseLong(datos[0]);
				long movieId = Long.parseLong(datos[1]);
				String tag = "";
				for (int i = 1; i < datos.length - 1; i++) {
					tag = tag + datos[i];
				}
				if (tag.startsWith("\"") && tag.endsWith("\"")) {
					tag = tag.substring(1, tag.length() - 1);
				}

				long timestamp = Long.parseLong(datos[datos.length-1]);
				
				VOTag t = new VOTag();
				t.setTag(tag);
				t.setMoveId(movieId);
				t.setUserId(userId);
				t.setTimestamp(timestamp);
				listaTags.agregarElementoFinal(t);
				cont++;
				if(cont%1000 == 0)System.out.println("Loading...");
			}
			res = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		agregarOperacionSR("cargarTags", time, System.currentTimeMillis());
		return res;
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		int i =listaPeliculas.darNumeroElementos();
		agregarOperacionSR("sizeMovies", time, System.currentTimeMillis());
		return i;

	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		int i =listaUsuarios.darNumeroElementos(); 
		agregarOperacionSR("sizeUsers", time, System.currentTimeMillis());
		return i;
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		int i = listaTags.darNumeroElementos();
		agregarOperacionSR("sizeTags", time, System.currentTimeMillis());
		return i;
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		crearListasGeneros();
		System.out.println("--------------Se generaron las listas de los generos--------------");
		asignarRatings();
		System.out.println("---------------------Se asignaron los ratings---------------------");
		ListaDobleEncadenada<VOGeneroPelicula> pop = new ListaDobleEncadenada<>();
		
		for(VOGeneroPelicula g : listaGeneros){
			quick.sort(g.getPeliculas(),0);
			VOGeneroPelicula ge = new VOGeneroPelicula();
			ge.setGenero(g.getGenero());
			ListaDobleEncadenada<VOPelicula> pe = new ListaDobleEncadenada<>();
			for(int i = 0;i<n&&i<g.getPeliculas().darNumeroElementos()-1;i++){
				pe.agregarElementoFinal(g.getPeliculas().darElemento(i));	
			}
			ge.setPeliculas(pe);
			pop.agregarElementoFinal(ge);;
			System.out.println("Loading...");
		}
		agregarOperacionSR("peliculasPopulares", time, System.currentTimeMillis());
		return pop;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		// TODO Auto-generated method s
		long time = System.currentTimeMillis();
		asignarRatings();
		quick.sort(listaPeliculas, QuickSort.COMPARACION_A�O_RATINGS_NOMBRE);
		agregarOperacionSR("catalogoPeliculasoerdenado", time, System.currentTimeMillis());
		return listaPeliculas;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		ListaDobleEncadenada<VOGeneroPelicula> rgp = new ListaDobleEncadenada<>();
		for(VOGeneroPelicula gp : listaGeneros){
			System.out.println("Loading...");
			quick.sort(gp.getPeliculas(), 0);
			ILista<VOPelicula> pelis = gp.getPeliculas();
			VOGeneroPelicula generoPelicula = new VOGeneroPelicula();
			generoPelicula.setGenero(gp.getGenero());
			ListaDobleEncadenada<VOPelicula> p = new ListaDobleEncadenada<>();
			int i =0;
			double mp = pelis.darElemento(i).getPromedioRatings();
			p.agregarElementoFinal(pelis.darElemento(i));
			while(pelis.darElemento(++i)!= null && pelis.darElemento(i).getPromedioRatings()==mp){
				p.agregarElementoFinal(pelis.darElemento(i));
			}
			generoPelicula.setPeliculas(p);
			rgp.agregarElementoFinal(generoPelicula);
			System.out.println("Loading...");
		}
		agregarOperacionSR("recomendarGenero", time, System.currentTimeMillis());
		return rgp;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		ListaDobleEncadenada<VOGeneroPelicula> orp = new ListaDobleEncadenada<>();
		asignarTags();
		crearListasGeneros();
		for(VOGeneroPelicula g : listaGeneros){
		quick.sort(g.getPeliculas(), QuickSort.COMPARACION_A�O_RATINGS_NOMBRE);
		orp.agregarElementoFinal(g);
		System.out.println("Loading...");
		}
		agregarOperacionSR("opinionRatingsGenero", time, System.currentTimeMillis());
		return orp;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		ListaDobleEncadenada<VOPeliculaPelicula> res = new ListaDobleEncadenada<>();
		Queue<VORating> cola = generarRecomendacion(rutaRecomendacion);
		VOPelicula p = new VOPelicula();
		VORating r = new VORating();
		ILista<VOPelicula> pelis = new ListaDobleEncadenada<>();
		VOPeliculaPelicula pp = new VOPeliculaPelicula();
		for(int i =0;i<n&&!cola.isEmpty();i++){
			r = cola.dequeue();
			p = buscarPeli(r.getIdPelicula());
			if(p!=null){
				for(int j = 0;j<listaGeneros.darNumeroElementos()-1;j++ ){
					if(listaGeneros.darElemento(j).getGenero().equals(p.getGenerosAsociados().darElemento(0))){
						pelis = listaGeneros.darElemento(j).getPeliculas();
					}
				}
				double rate = r.getRating();
				pp.setPelicula(p);
				pp.setPeliculasRelacionadas(new ListaDobleEncadenada<>());
				for(VOPelicula peliculaComp : pelis){
					if(peliculaComp.getPromedioRatings()>(rate-0.5)&&peliculaComp.getPromedioRatings()<(rate+0.5)){
						pp.getPeliculasRelacionadas().agregarElementoFinal(peliculaComp);
					}
				}
				res.agregarElementoFinal(pp);
			}
			System.out.println("Loading...");
		}
		agregarOperacionSR("recomendarPeliculas", time, System.currentTimeMillis());
		return res;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		boolean encontro = false;
		ILista<VORating> r = new ListaDobleEncadenada<>();
		for(int i =0;i<listaPeliculas.darNumeroElementos()-1&& !encontro;i++){
			VOPelicula p = listaPeliculas.darElemento(i);
			if(p.getIdPelicula()==idPelicula){
				encontro = true;
				for(VORating ra : listaRatings )
					if(ra.getIdPelicula()==p.getIdPelicula())
						r.agregarElementoFinal(ra);
			}
		}
		quick2.sort(r, 0);
		agregarOperacionSR("ratingsPeliculas", time, System.currentTimeMillis());
		return r;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		ILista<VOGeneroUsuario> lgu= new ListaDobleEncadenada<>();
		
		for(VOGeneroPelicula g: listaGeneros){
			VOGeneroUsuario gu = new VOGeneroUsuario();
			gu.setGenero(g.getGenero());
			ListaDobleEncadenada<VOUsuarioConteo> luc = new ListaDobleEncadenada<>();
			ILista<VOPelicula> gp = g.getPeliculas();
			for(VORating r : listaRatings){
				boolean encontroUsuario = false;
				
				for (int i =0;i<gp.darNumeroElementos()-1&&!encontroUsuario;i++){
					VOPelicula peli = gp.darElemento(i);
					
					if(peli.getIdPelicula()==r.getIdPelicula()){
						
						for (int j =0;j<listaUsuarios.darNumeroElementos()-1&&!encontroUsuario;j++){
							VOUsuario u = listaUsuarios.darElemento(j); 
							
							if(r.getIdUsuario()== u.getIdUsuario()){
								boolean encontro = false;
								for (int k =0;k<luc.darNumeroElementos()-1&&!encontro;k++){
									if(luc.darElemento(k).getIdUsuario()==u.getIdUsuario())encontro = true;;
								}
								if(!encontro){
									VOUsuarioConteo uc = new VOUsuarioConteo();
									uc.setConteo(u.getNumRatings());
									uc.setIdUsuario(u.getIdUsuario());
									luc.agregarElementoFinal(uc);
								}
								encontroUsuario = true;
							}
							
						}
					}
				}
			}
			gu.setUsuarios(luc);
			lgu.agregarElementoFinal(gu);
		}
		agregarOperacionSR("usuariosActivos", time, System.currentTimeMillis());
		return lgu;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		crearListaUsuarios();
		quick3.sort(listaUsuarios, 0);
		agregarOperacionSR("catalogoUsuariosOrdenado", time, System.currentTimeMillis());
		return listaUsuarios;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		ListaDobleEncadenada<VOGeneroPelicula> gp = new ListaDobleEncadenada<>();
		for(VOGeneroPelicula g : listaGeneros){
			int max = 0;
			VOGeneroPelicula gNew =  new VOGeneroPelicula();
			gNew.setGenero(g.getGenero());
			gNew.setPeliculas(new ListaDobleEncadenada<>());
			for(VOPelicula p : g.getPeliculas())if(p.getNumeroTags()>max)max=p.getNumeroTags();
			for(VOPelicula p : g.getPeliculas())if(p.getNumeroTags()==max)gNew.getPeliculas().agregarElementoFinal(p);
			gp.agregarElementoFinal(gNew);
		}
		agregarOperacionSR("recomendarTagsGenero", time, System.currentTimeMillis());
		return gp;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated stub
		long time = System.currentTimeMillis();
		ListaDobleEncadenada<VOUsuarioGenero> lug = new ListaDobleEncadenada<>();
		for(VOTag t : listaTags){
			boolean encontroUsuario = false;
			for(int i =0;i<lug.darNumeroElementos()&& encontroUsuario;i++){
				VOUsuarioGenero ug = lug.darElemento(i);
				if(t.getUserId()==ug.getIdUsuario()){
					ILista<VOGeneroTag> lgt = ug.getListaGeneroTags();
					boolean encontroGenero = false;
					for(int j =0;j<lgt.darNumeroElementos()&&!encontroGenero;j++){
						VOGeneroTag gt = lgt.darElemento(j);
						if(esDelGenero(t.getMoveId(), gt.getGenero())){
							gt.getTags().agregarElementoFinal(t.getTag());
							encontroGenero = true;
						}
					}
					if(!encontroGenero){
						VOPelicula p = buscarPeli(t.getMoveId());
						for(String g :p.getGenerosAsociados()){
							VOGeneroTag ngt = new VOGeneroTag();
							ngt.setGenero(g);
							ngt.setTags(new ListaDobleEncadenada<>());
							ngt.getTags().agregarElementoFinal(t.getTag());
							lgt.agregarElementoFinal(ngt);
						}
					}
					encontroUsuario = true;
				}
			}
			if(!encontroUsuario){
				VOUsuarioGenero nug = new VOUsuarioGenero();
				nug.setIdUsuario(t.getUserId());
				nug.setListaGeneroTags(new ListaDobleEncadenada<>());
				VOPelicula p = buscarPeli(t.getMoveId());
				for(String g :p.getGenerosAsociados()){
					VOGeneroTag ngt = new VOGeneroTag();
					ngt.setGenero(g);
					ngt.setTags(new ListaDobleEncadenada<>());
					ngt.getTags().agregarElementoFinal(t.getTag());
					nug.getListaGeneroTags().agregarElementoFinal(ngt);
				}
			}
		}
		agregarOperacionSR("opinionTagsGenero", time, System.currentTimeMillis());
		return lug;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null ;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		boolean encontro = false;
		ILista<VOTag> r = new ListaDobleEncadenada<>();
		for(int i =0;i<listaTags.darNumeroElementos()-1&& !encontro;i++){
			VOTag t = listaTags.darElemento(i);
			if(t.getMoveId()==idPelicula){
				r.agregarElementoFinal(t);
			}
		}
		quick4.sort(r, 0);
		agregarOperacionSR("tagsPelicula", time, System.currentTimeMillis());
		return r;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		VOOperacion op = new VOOperacion();
		op.setOperacion(nomOperacion);
		op.setTimestampInicio(tinicio);
		op.setTimestampFin(tfin);
		operaciones.push(op);
		agregarOperacionSR("agregarOperacion", time, System.currentTimeMillis());
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		Stack<VOOperacion> temp = new Stack<>();
		ILista<VOOperacion> l = new ListaDobleEncadenada<>();
		while(!operaciones.isEmpty()){
			VOOperacion o = operaciones.pop();
			l.agregarElementoFinal(o);
			temp.push(o);
		}
		while(!temp.isEmpty()){
			operaciones.push(temp.pop());
		}
		agregarOperacionSR("darHistorialdeOperaciones", time, System.currentTimeMillis());
		return l;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub
		operaciones = new Stack<>();
	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		Stack<VOOperacion> temp = new Stack<>();
		ILista<VOOperacion> l = new ListaDobleEncadenada<>();
		for(int i =0;i<n;i++){
			VOOperacion o = operaciones.pop();
			l.agregarElementoFinal(o);
			temp.push(o);
		}
		while(!temp.isEmpty()){
			operaciones.push(temp.pop());
		}
		agregarOperacionSR("darUltimasOperaciones", time, System.currentTimeMillis());
		return l;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		for(int i =0;i<n;i++)
			operaciones.pop();
		agregarOperacionSR("borrarUltimasOperaciones", time, System.currentTimeMillis());
	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		VOPelicula p = new VOPelicula();
		p.setAgnoPublicacion(agno);
		p.setTitulo(titulo);
		p.setGenerosAsociados(new ListaDobleEncadenada<>(generos));
		long id= 170000+(int)(Math.random()*100000);
		p.setIdPelicula(id);
		agregarOperacionSR("agregarPelicula", time, System.currentTimeMillis());
		return id;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		VORating r = new VORating();
		r.setIdUsuario(idUsuario);
		r.setIdPelicula(idPelicula);
		r.setRating(rating);
		r.setTimestamp(System.currentTimeMillis());
		listaRatings.agregarElementoFinal(r);
		agregarOperacionSR("agregarRating", time, System.currentTimeMillis());
	}
	
	/**
	 * Crea las listas de generos.
	 */
	public void crearListasGeneros(){
		listaGeneros = new ListaDobleEncadenada<>();
		for(VOPelicula p : listaPeliculas){

			for(String g : p.getGenerosAsociados()){
				boolean existe = false;
				for(int i =0;i<listaGeneros.darNumeroElementos()-1&&!existe;i++){
					VOGeneroPelicula ge = listaGeneros.darElemento(i);
					if(ge.getGenero().equals(g)){
						ge.getPeliculas().agregarElementoFinal(p);
						existe = true;
					}
				}
				if(!existe){
					VOGeneroPelicula ng = new VOGeneroPelicula();
					ListaDobleEncadenada<VOPelicula> listaP = new ListaDobleEncadenada<>();
					ng.setGenero(g);
					listaP.agregarElementoFinal(p);
					ng.setPeliculas(listaP);
					listaGeneros.agregarElementoFinal(ng);
				}
			}
		}

	}
	
	
	/**
	 * Asigna los ratings de la lista de raitings a la lista de peliculas
	 */
	public void asignarRatings(){
		for(VOPelicula p : listaPeliculas){
			long id = p.getIdPelicula();
			for(VORating r: listaRatings){
				if(r.getIdPelicula() == id){
					int n = p.getNumeroRatings();
					double prom = p.getPromedioRatings();
					prom = ((prom*n)+r.getRating())/(n+1);
					p.setPromedioRatings(prom);
					p.setNumeroRatings(n+1);
				}
			}
		}
	}
	
	public void asignarTags(){
		int cont =0;
		for(VOPelicula p : listaPeliculas){
			long id = p.getIdPelicula();
			for(VOTag t: listaTags){
				if(t.getMoveId() == id){
					p.getTagsAsociados().agregarElementoFinal(t);
					p.setNumeroRatings(p.getNumeroTags()+1);
				}
			}
			cont ++;
			if(cont%1000==0)System.out.println("Cargando Tags...");
		}
		System.out.println("----------Los Tags fueron asignados correctamente----------");
	}
	
	public Queue<VORating> generarRecomendacion(String ruta){
		Queue<VORating> ts = new Queue<>();
		BufferedReader buff = null;
		String sepCvs = ",";
		String line = "";
		try {
			buff = new BufferedReader(new FileReader(ruta));
			line = buff.readLine();
			while ((line = buff.readLine()) != null) {
				String[] datos = line.split(sepCvs);
				long movieId = Long.parseLong(datos[0]);
				double rating = Double.parseDouble(datos[1]);
				
				VORating r = new VORating();
				r.setIdPelicula(movieId);
				r.setIdUsuario(-1);
				r.setRating(rating);
				r.setTimestamp(0);
				ts.enqueue(r);
			}
			buff.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ts;
	}
	
	public void crearListaUsuarios(){
		listaUsuarios = new ListaDobleEncadenada<>();
		for(VORating r : listaRatings){
			boolean existe = false;
			for(int i =0;i<listaUsuarios.darNumeroElementos()-1&&!existe;i++){
				VOUsuario u = listaUsuarios.darElemento(i);
				if(r.getIdUsuario()==u.getIdUsuario()){
					u.setNumRatings(u.getNumRatings()+1);
					if(r.getTimestamp()<u.getPrimerTimestamp())u.setPrimerTimestamp(r.getTimestamp());
					existe = true;
				}
			}
			if(!existe){
				VOUsuario us = new VOUsuario();
				us.setIdUsuario(r.getIdUsuario());
				us.setNumRatings(1);
				us.setPrimerTimestamp(r.getTimestamp());
				listaUsuarios.agregarElementoFinal(us);
			}
		}
		
		for(VOTag t : listaTags){
			boolean existe = false;
			for(int i =0;i<listaUsuarios.darNumeroElementos()-1;i++){
				VOUsuario u = listaUsuarios.darElemento(i);
				if(t.getUserId()==u.getIdUsuario()){
					if (t.getTimestamp()<u.getPrimerTimestamp()) 
						u.setPrimerTimestamp(t.getTimestamp());
					existe = true;
				}
			}
			if(!existe){
				VOUsuario us = new VOUsuario();
				us.setIdUsuario(t.getUserId());
				us.setNumRatings(0);
				us.setPrimerTimestamp(t.getTimestamp());
				listaUsuarios.agregarElementoFinal(us);
			}
		}		
	}
	
	public boolean esDelGenero(long id,String genero){
		boolean encontro = false;
		for(int i =0;i<listaGeneros.darNumeroElementos()-1&&!encontro;i++){
			if(listaGeneros.darElemento(i).getGenero()==genero){
				ILista<VOPelicula> lgp = listaGeneros.darElemento(i).getPeliculas();
				for(int j =0;j<lgp.darNumeroElementos()-1;j++){
					if(lgp.darElemento(j).getIdPelicula()==id)return true;
				}
			}
		}
		return false;
	}

	public VOPelicula buscarPeli(long id){
		for(VOPelicula p : listaPeliculas)
			if(p.getIdPelicula()==id)
				return p;
		return null;
	}

	@Override
	public void printAll() {
		// TODO Auto-generated method stub
		for(VOPelicula p: listaPeliculas){
		System.out.println(p.getAgnoPublicacion() + " : " + p.getIdPelicula() + " : " + p.getTitulo() + " : " + p.getPromedioRatings());	
		}
	}
}
