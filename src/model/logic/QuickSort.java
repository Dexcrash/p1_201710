package model.logic;

import model.data_structures.ILista;
import model.vo.VOPelicula;

public class QuickSort<T extends Comparable<T>> {

	public static final int COMPARACION_A�O_RATINGS_NOMBRE = 1;
	
	private int estado;
	
	public void sort(ILista<T> a, int i){
		sort(a, 0, a.darNumeroElementos()-1);
		estado = i;
	}

	public void sort(ILista<T> a ,int lo, int hi){
		if(hi <= lo) return;
		int j = partition(a, lo, hi);
		sort(a, lo, j-1);
		sort(a, j+1, hi);
	}
	
	public int partition(ILista<T> lista, int lo, int hi) {
		int i = lo, j = hi + 1;
		T p = lista.darElemento(lo);
		while (true) {
			while (less(lista.darElemento(++i), p))if (i == hi)break;
			while (less(p, lista.darElemento(--j)))if (j == lo)break;
			if (i >= j)break;
			exch(lista, i, j);
		}
		exch(lista, lo, j);
		return j;
	}

	public void exch(ILista<T> lista, int a, int b) {
		T t = lista.darElemento(a);
		lista.setItem(lista.darElemento(b),a);
		lista.setItem(t,b);
	}

	public boolean less(T a, T b) {
		if(estado == COMPARACION_A�O_RATINGS_NOMBRE){
			if((((VOPelicula) a).getAgnoPublicacion())<(((VOPelicula)b).getAgnoPublicacion())){
				return true;
			}
			else if((((VOPelicula) a).getAgnoPublicacion())==(((VOPelicula)b).getAgnoPublicacion())){
				if((((VOPelicula) a).getPromedioRatings())<(((VOPelicula)b).getPromedioRatings())){
					return true;
				}
				else if((((VOPelicula) a).getPromedioRatings())==(((VOPelicula)b).getPromedioRatings())){
					if((((VOPelicula) a).getTitulo().compareTo(((VOPelicula)b).getTitulo()))<0){
						return true;
					}
				}
			}
			return false;
		}
		else{
			return a.compareTo(b) < 0;
		}
	}

	public void show(ILista<T> a) {
		for (T t : a)
			System.out.print(t + "  ");
		System.out.println();
	}

	public boolean isSorted(ILista<T> a) {
		for (int i = 0; i < a.darNumeroElementos(); i++)
			if (less(a.darElemento(i),a.darElemento(i-1)))
				return false;
		return true;
	}

}
